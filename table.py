#!/usr/bin/env python3

#  Copyright Ludovic Kiefer 2021

#  This file is part of Gnu Heads.

#  Gnu Heads is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 2 of the License, or
#  (at your option) any later version.

#  Gnu Heads is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.

#  You should have received a copy of the GNU General Public License
#  along with Gnu Heads.  If not, see <https://www.gnu.org/licenses/>.

import card
import player
import random
import json
import asyncio
from card import *

class Table:

    """A table of the game Gnu Heads"""

    def __init__(self, player, code, password=None, deckSize=104, lineAmount=4, lineLength=6, point=66):
        self.id = code
        self.password = password
        self.point = point
        self.lineAmount = lineAmount if 2 <= lineAmount <= 5 else 4
        self.lineLength = lineLength if 2 <= lineLength <= 8 else 6
        self.deckSize = deckSize if 60 <= deckSize <= 104 else 104
        self.status = "wait"
        self.placeList = {} # key: Cards to place on the table; Value: player
        # wait: waiting for players
        # play: the game was started by the owner
        # place_card: placing the cards on the table
        # select_line: waiting for a user to select a line
        # end: the game is finished

        # Store the card points
        self.deck = {}
        for card in range(1, self.deckSize+1):
            self.deck[card] = getPoint(card)
        
        # All players at this table
        self.playerList = []
        # The first Player in the list will be the owner of the table
        self.addPlayer(player)
        
        # The four lines of cards
        self.line = []
        for i in range(self.lineAmount):
            self.line.append([])

        # Scores at the end of the game
        #self.score = []
        self.pointList = []

    async def start(self):
        self.distributeCard()
        # Reset points
        for p in self.playerList:
            p.setPoint(self.point)
        self.setStatus("play")
        await self.sendData()
        for p in self.playerList:
            await p.sendPlayerData()

    def addPlayer(self, player):
        if player not in self.playerList:
            self.playerList.append(player)

    def removePlayer(self, player):
        self.placeList = {}
        try:
            self.playerList.remove(player)
        except:
            pass
    
    def getPlayerList(self):
        return self.playerList

    def getId(self):
        return self.id

    def getLineAmount(self):
        return self.lineAmount

    def getPoint(self):
        return self.point

    def getPointList(self):
        return self.pointList

    def verifyPassword(self, password):
        return self.password == password

    async def send(self, message):
        for p in self.playerList:
            await p.send(message)

    def getStatus(self):
        return self.status

    def setStatus(self, status):
        if status in ("wait", "play", "end", "place_card", "select_line"):
            self.status = status

    def getOwner(self):
        if self.playerList:
            return self.playerList[0]
        return None

    async def sendData(self):
        for p in self.playerList:
            msg = json.dumps({"table": self.getData()})
            await self.send(msg)
            if self.status == "end":
                # Send the final result
                msg = json.dumps({"end": self.pointList})
                await table.send(msg)

    def distributeCard(self):
        deck = []
        for i in range(1,self.deckSize+1):
            deck.append(i)
        random.shuffle(deck)
        # Distribute cards to all players
        for p in self.playerList:
            p.setHand(sorted(deck[-10:]))
            deck = deck[0:-10]
        # Place 4 cards on the table
        for l in self.line:
            l.clear()
            l.append(deck.pop())

    async def placeCard(self, playerId = -1) -> bool:
        "Place the cards on the table"
        # Returns True if the cards are placed on the table

        # Verify that all player has a card selected
        # and generate the list of cards
        if not self.placeList:
            # compute the list of card to place
            placeList = {}
            for p in self.playerList:
                card = p.getSelectedCard()
                if not card:
                    return False # One player doesn't have a card selected: abort
                placeList[card] = p
            self.placeList = placeList # Cache the list in case a player must choose a line
        self.setStatus("place_card")

        # Placing all cards, sorted by value
        actionList = [] #
        for c in sorted(self.placeList):
            selectedLine = None # Will point to a line number
            p = self.placeList[c] # p is the player associated to the card c
            gap = 1000 # value difference between two cards, 1000 is the maximum
            for l in range(len(self.line)): # l = line number
                if self.line[l][-1] < c and c - self.line[l][-1] < gap:
                    selectedLine = l
                    gap = c - self.line[l][-1]

            # Unable to choose a line automatically, do the player choose a line?
            if selectedLine == None and p.getSelectedLine() in range(self.lineLength):
                selectedLine = p.getSelectedLine()
                p.removePoint(self.countPoint(self.line[selectedLine]))
                self.line[selectedLine].clear()
                msg = json.dumps({"table": self.getData()})
                await self.send(msg)

            # Place the card into the selected line
            if selectedLine in range(self.lineLength):
                line=self.line[selectedLine]
                # Place the card into the line
                actionList.append({"card": c, "id": p.getId(), "name": p.getName(), "point": p.getPoint(), "line": selectedLine})
                if len(line) == self.lineLength-1:
                    # The player fill the line and lose points
                    p.removePoint(self.countPoint(self.line[selectedLine]))
                    line.clear()
                line.append(c)
                p.removeCard(c)
            # No line selected, ask the player
            else:
                self.setStatus("select_line")
                await self.sendSelectLine()
                return False
            p.setSelectedLine(None)
        self.placeList = {}

        # if players hands are empty
        if len(self.playerList[0].getHand()) == 0:
            # If one player score is below 1, game is finished
            for p in self.playerList:
                if p.getPoint() <= 0:
                    # Game is finished
                    self.setStatus("end")
                    self.recordPoint()
            
            # No player have above 1 point, the game continue
            if self.getStatus() != "end":
                self.distributeCard()

        # Send to all players the card to be placed on the table
        await self.send(json.dumps({"place_card": actionList}))

        return True

    def countPoint(self, line) -> int:
        point = 0
        for card in line:
            point += self.deck[card]
        return point

    async def sendSelectLine(self, player=None):
        # Generate a list of card and player and send it to all players
        # The player with the smallest card will select a line
        cardList = []
        for c in sorted(self.placeList):
            p = self.placeList[c]
            cardList.append({"card": c, "id": p.getId(), "name": p.getName(), "point": p.getPoint()})
        if player:
            await player.send(json.dumps({"select_line": cardList}))
        else:
            await self.send(json.dumps({"select_line": cardList}))

    def recordPoint(self):
        "Record the points of all players into an array"
        winnerList = sorted(self.playerList, key=lambda x: x.point, reverse=True)
        pointList = []
        for p in winnerList:
            pointList.append( {"id": p.getId(), "name": p.getName(), "point": p.getPoint()} )
        self.pointList = pointList

    def getData(self):
        # Generate a report including all **public** data of the table
        playerListData = []
        tableData = []
        for p in self.playerList:
            playerListData.append({"id": p.getId(), "name": p.getName(), "point": p.getPoint(), "card": p.isSelectedCard(), "status": p.getStatus()})
        if not self.getOwner():
            ownerId = None
        else:
            ownerId = self.getOwner().getId()
        tableData = {
            "player_list": playerListData,
            "id": self.id,
            "status": self.status,
            "owner": ownerId,
            "line_list": self.line,
            "line_amount": self.lineAmount,
            "line_length": self.lineLength,
            "deck_size": self.deckSize,
            "deck": self.deck,
            #"place_list": self.placeList
        }
        return tableData

    def __str__(self):
        return "Table #" + str(self.id) + ", " + str(len(self.playerList)) + " joueur(s)"

    def __del__(self):
        print ("The table " + str(self.id) + " is deleted.")


if __name__ == "__main__":
    pass
