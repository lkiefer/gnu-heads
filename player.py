#!/usr/bin/env python3

#  Copyright Ludovic Kiefer 2021

#  This file is part of Gnu Heads.
  
#  Gnu Heads is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 2 of the License, or
#  (at your option) any later version.
  
#  Gnu Heads is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
  
#  You should have received a copy of the GNU General Public License
#  along with Gnu Heads.  If not, see <https://www.gnu.org/licenses/>.
    
import card
import asyncio
import itertools
import secrets
import json

class Player:

    "A player of Gnu Heads"
    id_iter = itertools.count(1)

    def __init__(self, websocket, name=""):

        self.point = 66
        self.hand = [] # List of integers (card numbers)
        self.setName(name)
        self.id = next(self.id_iter) # This id may be replaced when the player reconnects
        self.websocket = websocket
        self.table = None
        self.selectedCard = None
        self.selectedLine = None
        self.status = "connected"
        self.token = secrets.token_hex(32)

    def setHand(self, hand):
        self.hand = hand

    def removeCard(self, card: int):
        if self.selectedCard == card:
            self.selectedCard = None
        self.hand.remove(card)

    def removePoint(self, point: int):
        self.point -= point

    def setPoint(self, point):
        self.point = point

    def getPoint(self):
        return self.point

    def getHand(self):
        return self.hand

    def getName(self):
        return self.name

    def setName(self, name):
        self.name = name

    def getId(self):
        return self.id

    def getSelectedCard(self):
        return self.selectedCard

    def setSelectedCard(self, card):
        if card in self.hand or card == None:
            self.selectedCard = card
            return True
        else:
            return False

    def isSelectedCard(self):
        return self.selectedCard != None

    def getSelectedLine(self):
        return self.selectedLine

    def setSelectedLine(self, line: int) -> bool:
        try:
            lineAmount = self.table.getLineAmount()
            if line == None or line in range(lineAmount):
                self.selectedLine = line
                return True
            else:
                return False
        except:
            return False

    def setTable(self, table):
        # Check if the player is at another table, then leave it
        if self.table and self.table != table:
            self.table.removePlayer(self)
        self.table = table

    def unsetTable(self):
        oldTable = self.table
        if self.table:
            self.table.removePlayer(self)
        self.table = None
        self.hand = []
        self.selectedCard = None
        self.selectedLine = None
        return oldTable

    def getTable(self):
        return self.table

    def getTableId(self):
        if self.table:
            return self.table.getId()

    def disconnect(self):
        self.status = "disconnected"

    def reconnect(self):
        self.status = "connected"

    def getToken(self):
        return self.token

    def getStatus(self):
        return self.status

    def setWebsocket(self, ws):
        self.websocket = ws

    def getWebsocket(self):
        return self.websocket

    def getData(self):
        data = {
        "set_player": {"id": self.id,
        "token": self.token,
        "table": self.getTableId(),
        "name": self.name,
        "hand": self.hand,
        "selected": self.selectedCard,
        }}
        return data

    async def sendPlayerData(self):
        await self.send(json.dumps(self.getData()))

    async def send(self, message):
        try:
            await self.websocket.send(message)
        except:
            print("Player " + str(self.id) + " connection unavailable")
            self.websocket = None

    def __del__(self):
        print ("The player " + str(self.id) + " is deleted.")

    def __str__(self):
        return "The player " + str(self.id) + " (" + self.name + ") has " + str(self.point)  + " points and " + str(len(self.hand)) + "card(s)."

if __name__ == "__main__":
    pass
