/* Copyright Ludovic Kiefer 2021
 * This file is part of Gnu Heads.
 * 
 * Gnu Heads is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * Gnu Heads is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Gnu Heads.  If not, see <https://www.gnu.org/licenses/>.
 */

const i18n = VueI18n.createI18n({
  locale: 'fr',
  fallbackLocale: 'en',
  messages,
})

const appdata = {
  data() {
    return {
      debug: false,
      show_next: undefined,
      show_startpage: true,
      show_choosetable: false,
      show_createtable: false,
      show_table: false,
      show_no_connection: true,
      tableId: '',
      tableOwner: '',
      tablePassword: '',
      startPoint: 70,
      deckSize: 100,
      lineAmount: 4,
      lineLength: 5,
      tableStatus: '',
      playerList: [],
      lineList: [],
      placeCardList: [], // index: player, value: card, line: line
      placeCardOrder: [], // Which player will place the card
      selectedCardList: [], // Cards choosen by the players
      hand: [],
      address: location.host,
      port: "6789",
      playerName: '',
      playerId: undefined,
      inputPlayerName: '',
      playerToken: undefined,
      endPoint: [],
      preloadedCard: 0, //which card is loaded
      // create a local.js file to customize vm.m1, vm.m2 and vm.m3 vars
      // contact e-mail
      m1: 'contac',
      m2: 't@e',
      m3: 'xample.com',
    }
  },
  methods: {
    showPage: function(page) {
      this.show_startpage = false;
      this.show_choosetable = false;
      this.show_createtable = false;
      this.show_table = false;
      this[page] = true;
    },
    recoverConnexion: function() {
      if (!this.playerId  || !this.playerToken) {
        this.playerId = document.cookie
          .split('; ')
          .find(row => row.startsWith('player_id'))
          .split('=')[1];
        this.playerToken = document.cookie
          .split('; ')
          .find(row => row.startsWith('player_token'))
          .split('=')[1];
      }
      if (this.playerId && this.playerToken){
        var msg = {reconnect: {player_id: this.playerId,
          player_token: this.playerToken}};
        this.sendMessage(msg);
      } else {
      }
    },
    setPlayerName: function() {
      var msg = {set_player_name: this.inputPlayerName};
      this.sendMessage(msg);
    },
    joinTable: function(table, password) {
      var msg = {join_table: {table_id: this.tableId,
        table_password: this.tablePassword}};
      this.sendMessage(msg);
    },
    quitTable: function() {
      var msg = {quit_table: true};
      this.sendMessage(msg);
    },
    startTable: function() {
      var msg = {start_table: true};
      this.sendMessage(msg);
    },
    createTable: function() {
      var msg = {create_table: {
        password: this.tablePassword,
        deck_size: Number(this.deckSize),
        line_amount: Number(this.lineAmount),
        line_length: Number(this.lineLength),
        point: Number(this.startPoint)} };
      this.sendMessage(msg);
    },
    selectCard: function(c) {
      var msg = {select_card: c};
      this.sendMessage(msg);
    },
    selectLine: function(l) {
      var msg = {select_line: l};
      this.sendMessage(msg);
    },
    sendMessage: function(message) {
      this.ws.send(JSON.stringify(message));
    },
    preloadCardAsset: function() {
      // Preload card until the decksize is reached
      if (this.preloadedCard < this.deckSize){
        var img = new Image();
        this.preloadedCard++;
        var src = "cards/png/" + this.preloadedCard + ".png";
        img.src = src;
        img.onload = this.preloadCardAsset;
      }
    },
    // Functions below are for actions initiated by the server
    setPlayer: function(p) {
      if (this.playerToken !== undefined && this.playerToken != p.token){
        // We get a new player (maybe the server is reloaded or the connection timed out)
        // This player is not yet playing
        this.quitTableAction();
      }
      this.playerName = p.name;
      this.inputPlayerName = p.name;
      this.playerId = p.id;
      this.playerToken = p.token;
      this.hand = p.hand;
      this.table = p.table;
      this.selected = p.selected
      document.cookie = "player_token=" + p.token + ";max-age=172800";
      document.cookie = "player_id=" + p.id + ";max-age=172800";

      if (this.hand.length == 1){
        // It is the last card, then play it!
        this.selectCard(this.hand[0])
      }

      if (this.show_next){
        this.showPage(this.show_next);
        this.show_next = undefined;
      }
    },
    placeCard: function(data) {
      this.tableStatus = "place_card";
      this.selectedCardList = [];
      this.selectedCardList = data;
      // Animation
      var delay=2000;
      this.selectedCardList.forEach(function(item){

        // Resize the card
        setTimeout(function(){
          var col = vm.lineList[item.line].length
          var start = document.getElementById("card-" + item.card);
          var stop = document.getElementById("line-" + item.line +
            "-card-" + col );

          // Resize the card
          start.style.height = stop.height + "px";
          start.style.width = stop.width + "px";
        }, delay);

        // Move the card
        setTimeout(function(){
          var col = vm.lineList[item.line].length
          var start = document.getElementById("card-" + item.card);
          var stop = document.getElementById("line-" + item.line +
            "-card-" + col );
          // Compute the displacement
          var x = stop.getBoundingClientRect().x - start.getBoundingClientRect().x;
          var y = stop.getBoundingClientRect().y - start.getBoundingClientRect().y;
          // Move the card
          start.style.zIndex = "10";
          start.style.transform = "translate("+ x +"px, "+ y +"px)";
          start.style.zIndex = "auto";
        }, delay+400);

        // Add the card to the line
        setTimeout(function(){
          // When the card animation is finished, add the card to the lineList
          vm.lineList[item.line].push(item.card);
          // Hide the card that was moving
          var cardElement = document.getElementById("card-" + item.card);
          cardElement.style.display = 'none';
          // last card in the line: clear the line
          if (vm.lineList[item.line].length === vm.lineLength){
            // Clear the line, set the first card
            card = vm.lineList[item.line].pop();
            vm.lineList[item.line] = [];
            vm.lineList[item.line].push(card);
          }
        }, delay+1500);
        delay += 2000;
        });
    },
    setSelectedCardList: function(data) {
      this.selectedCardList = data;
      this.tableStatus = "select_line";
    },
    setTable: function(data) {
      if (data.id !== undefined){
        this.tableId = data.id;
        this.tableStatus = data.status;
        this.tableOwner = data.owner;
        this.playerList = data.player_list;
        this.lineList = data.line_list;
        this.deckSize = data.deck_size;
        this.lineAmount = data.line_amount;
        this.lineLength = data.line_length;
        if (this.tableStatus !== "select_line"){
          this.selectedCardList = [];
        }
        this.showPage('show_table');
        this.preloadCardAsset();
      }
    },
    endRound: function(data) {
      this.endPoint = data;
    },
    quitTableAction: function(data) {
      this.hand = [];
      this.tableId = '';
      this.tableOwner = '';
      this.tablePassword = '';
      this.tableStatus = '';
      this.playerList = [];
      this.lineList = [];
      this.placeCardList = [];
      this.placeCardOrder = [];
      this.selectedCardList = [];
      this.endPoint = [];
      this.showPage('show_startpage');
    },
    onmessageAction: function(event) {
      data = JSON.parse(event.data);
      if (this.debug == true){
        // Set vm.debug to true in your browser to show all received messages
        console.log(event.data);
      }
      if (data.set_player !== undefined){
        this.setPlayer(data.set_player);
      } else if (data.table !== undefined) {
        this.setTable(data.table);
      } else if (data.place_card !== undefined){
        this.placeCard(data.place_card);
      } else if (data.select_line !== undefined) {
        this.setSelectedCardList(data.select_line);
      } else if (data.quit_table !== undefined) {
        this.quitTableAction(data.quit_table);
      } else if (data.end !== undefined){
        this.endRound(data.end);
      }
    },
    onopenAction: function() {
      this.show_no_connection = false;
      this.recoverConnexion();
    },
    oncloseAction: function(){
      this.show_no_connection = true;
      
      // Try a connection every 3 seconds
      setTimeout(function(){
        if (vm.ws.readyState === WebSocket.CLOSED) {
          vm.createWebSocket();
        }
      }, 3000);
    },
    createWebSocket: function() {
      this.ws = new WebSocket("ws://" + this.address + ":" + this.port + "/")
      this.ws.onclose = this.oncloseAction
      this.ws.onopen = this.onopenAction
      this.ws.onmessage = this.onmessageAction
    }
  },
  mounted() {
    this.createWebSocket()
  }
}

const app = Vue.createApp(appdata);


app.component('player-list-default', {
  props: ['list'],
  template: `
    <template v-for="p in list">
      <div>
        <div class="littlecard" v-if="p.card">
          <img class="littlecard" v-bind:src="'cards/png/empty.png'" >
        </div>
        <div class="littlecard" v-else>
          <img class="littlecard" v-bind:src="'cards/png/wait.png'" >
        </div>
        <div class="littlecard"></div>
        {{ p.name }} {{ p.point }}pts
        <span v-if="p.status === 'disconnected'">❌</span>
      </div>
    </template>
    `
})

app.component('player-list-place', {
  props: ['list', 'tableStatus'],
  template: `
    <template v-for="p, i in list">
      <div>
        <div class="littlecard">
          <img class="littlecard" v-bind:src="'cards/png/' + p.card + '.png'" v-bind:id="'card-' + p.card">
        </div>
        <div class="littlecard">
          <img  v-if="tableStatus === 'select_line' && i === 0" class="littlecard" src="cards/png/wait.png">
        </div>
        {{ p.name }} {{ p.point }}pts
        <template v-if="p.status === 'disconnected'">❌</template>
      </div>
    </template>
    `
})

app.component('game-view', {
  props: ['hand','selected', 'selectedCardList', 'playerList', 'tableStatus', 'tableOwner', 'playerId', 'lineList', 'lineLength', 'endPoint', 'tableId', 'deckSize'],
  emits: ['startTable', 'quitTable', 'selectCard', 'selectLine'],
  template: `
      <div id="top-bar">
        <template v-if="hand.length !== 0">
        <div id="hand">
          <span v-for="c in hand">
            <img class="card card-unselected" v-if="c !== selected && selected != undefined"
              @click="$emit('selectCard', c)" v-bind:src="'cards/png/' + c + '.png'">
            <img class="card" v-else-if="selected == undefined"
              @click="$emit('selectCard', c)" v-bind:src="'cards/png/' + c + '.png'">
            <img class="card" v-else  v-bind:src="'cards/png/' + c + '.png'">
          </span>
        </div></template><template v-else-if="endPoint[0] === undefined">
        <div id="hand">
          <p v-if="tableOwner === playerId && playerId != undefined">{{ $t('table.wait_status_owner') }}</p>
          <p v-else>{{ $t('table.wait_status', { msg: playerList[0].name }) }}</p>
        </div></template><template v-else>
        <div id="hand">
          <p v-if="tableOwner === playerId && playerId != undefined">{{ $t('table.end_status_owner') }}</p>
          <p v-else>{{ $t('table.end_status', { msg: playerList[0].name }) }}</p>
        </div></template><div id="table-controls">
            Table: {{ tableId }}<br>
          <span v-if="tableOwner === playerId && playerId != undefined">
            <button @click="$emit('startTable')" v-if="tableStatus === 'wait'">{{ $t('btn_start') }}</button>
            <button @click="$emit('startTable')" v-if="tableStatus === 'end'">{{ $t('btn_restart') }}</button>
          </span>
          <button @click="$emit('quitTable')">{{ $t('btn_quit') }}</button>
        </div>
      </div>
      <div id="table-view">

        <!-- Cards on the table -->
        <div v-if="tableStatus !== 'end' && tableStatus !== 'wait'">
          <div v-for="l, lineIndex in lineList" v-bind:id="'line-' + lineIndex">
            <template v-for="i in lineLength">
              <img class="card" v-if="l[i-1] && i<=lineLength" v-bind:src="'cards/png/' + l[i-1] + '.png'"
              v-bind:id="'line-' + lineIndex + '-card-' + (i-1)">
              <img class="card" v-else-if="i!==lineLength" v-bind:src="'cards/png/empty.png'"
              v-bind:id="'line-' + lineIndex + '-card-' + (i-1)">
              <img class="card" v-else v-bind:src="'cards/png/last.png'" v-bind:id="'line-' + lineIndex + '-card-' + (i-1)">
              <img class="card" v-if="i===lineLength && selectedCardList[0] && selectedCardList[0].id === playerId && tableStatus === 'select_line'"
                @click="$emit('selectLine', lineIndex)" v-bind:src="'cards/png/arrow.png'" >
            </template>
          </div>
        </div>

        <!-- Message if the game is finished -->
        <div v-else-if="tableStatus === 'end' && endPoint[0]" id="winner-view">
          <h2>The winner is {{ endPoint[0].name }}!</h2>
          <p v-for="p, i in endPoint">#{{ i }} {{ p.point }}pts {{ p.name }} </p>
        </div>

        <!-- Waiting -->
        <div v-if="tableStatus === 'wait' || tableStatus === 'end'" class="center">
          <p>{{ $t('table.decksize') }}<br>
          <img class="card" v-bind:src="'cards/png/' + deckSize + '.png'"></p>
        </div>

      </div>

      <div id="player-tab"><!--˟❌❎⨯⏳⌛-->
        <!-- List players sorted by Id-->
        <template v-if="selectedCardList.length === 0">
          <player-list-default :list="playerList"></player-list-default>
        </template>
        <!-- When placing cards, list players by card value -->
        <template v-else>
          <player-list-place :list="selectedCardList" :table-status="tableStatus"></player-list-place>
        </template>
      </div>
  `
})


app.use(i18n)
const vm = app.mount('#app');
