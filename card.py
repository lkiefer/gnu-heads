#!/usr/bin/env python3

#  Copyright Ludovic Kiefer 2021

#  This file is part of Gnu Heads.

#  Gnu Heads is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 2 of the License, or
#  (at your option) any later version.

#  Gnu Heads is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.

#  You should have received a copy of the GNU General Public License
#  along with Gnu Heads.  If not, see <https://www.gnu.org/licenses/>.



def getPoint(card: int) -> int:
    """Get the points for each card"""

    if card in (32, 64, 128, 256):
        return 7
    if card % 16 == 0:
        return 5
    if card % 4 == 0:
        return 3
    if card % 2 == 0:
        return 2
    else:
        return 1
