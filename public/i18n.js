const messages = {
  en: {
    
      start: {
        language: "Language:",
        title: "Choose a name",
        input_name: "Name:",
        a_table: "a table.",
      },
      create: {
        title: 'Create a new table',
        hello: 'Hello {msg}! Choose a password and click on "Create". A new table number will be generated, give it to your friends!',
        input_cards: 'Amount of cards:',
        input_line_amount: 'Amount of lines:',
        input_line_length: 'Length of the lines:',
        input_point: 'Points at the beginning:',
      },
      join: {
        title: 'Join a table',
        hello: 'Hello {msg}! Please set the table number and the password (optional), then click on "Join".',
        input_table_name: 'Table number:',
      },
      table: {
        wait_status: 'When all players are ready, {msg} will start the game.',
        wait_status_owner: 'Click on start when all players are ready.',
        end_status: '{msg} may choose to invite new players and restart the game.',
        end_status_owner: 'You can invite new players and restart the game.',
        decksize: 'In this round, the biggest card would be:',
      },
      modal: {
        connection_error: "<p>The game server is unreachable. Please check your Internet connection, wait a moment and refresh the page.</p>" +
        "<p>Some network restrict communications, especially offices and schools. If it's your case, try from another network." +
        "<p>If problem persist, you can join me at {0}{1}{2}</p>",
      },
      btn_create: 'Create',
      btn_join: 'Join',
      btn_quit: 'Quit',
      btn_back: 'Back',
      btn_cancel: 'Cancel',
      btn_start: 'Start',
      btn_restart: 'Restart',
      or: 'or',
      input_password: 'Password:',
      language_name: 'English',
    
  },
  fr: {
    
      start: {
        language: "Langue :",
        title: "Choisissez un nom :",
        input_name: "Nom :",
        a_table: "une table.",
      },
      create: {
        title: 'Créer une nouvelle table',
        hello: 'Bonjour {msg}! Choisissez un mot de passe, et cliquez sur « Créer ». Un nouveau numéro de table sera créé, passez le à vos amis!',
        input_cards: 'Nombre de cartes :',
        input_line_amount: 'Nombre de lignes :',
        input_line_length: 'Longueur des lignes :',
        input_point: 'Points au départ :',
      },
      join: {
        title: 'Rejoindre une table',
        hello: 'Bonjour {msg}! Indiquez le numéro de la table et le mot de passe (optionnel) puis cliquez sur « Rejoindre ».',
        input_table_name: 'Numéro de table :',
      },
      table: {
        wait_status: '{msg} lancera la partie quand tous les joueurs seront présents.',
        wait_status_owner: 'Cliquez sur Démarrer dès que les joueurs sont prêts.',
        end_status: '{msg} peut inviter d\'autres joueurs et relancer la partie.',
        end_status_owner: 'Vous pouvez inviter de nouveaux joueurs et relancer la partie.',
        decksize: 'Durant cette partie, la carte la plus élevée pourra être :',
      },
      modal: {
        connection_error: "<p>Le serveur de jeu est injoignable. Vérifiez votre connexion à Internet, patientez un moment et actualisez la page.</p>" +
        "<p>Certains réseaux bloquent certaines communication, notamment dans les réseaux d'entreprises, " +
        "les écoles et réseaux publics. Si vous êtes dans ce cas, veuillez essayer depuis un autre réseau.</p>" +
        "<p>Si le problème persiste, vous pouvez me joindre à l'adresse {0}{1}{2}</p>",
      },
      btn_create: 'Créer',
      btn_join: 'Rejoindre',
      btn_quit: 'Quitter',
      btn_back: 'Retour',
      btn_cancel: 'Annuler',
      btn_start: 'Démarrer',
      btn_restart: 'Recommencer',
      or: 'ou',
      input_password: 'Mot de passe :',
      language_name: 'Français',
      
  }
}
