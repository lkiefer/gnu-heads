#!/usr/bin/env python3

#  Copyright Ludovic Kiefer 2021

#  This file is part of Gnu Heads.

#  Gnu Heads is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 2 of the License, or
#  (at your option) any later version.

#  Gnu Heads is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.

#  You should have received a copy of the GNU General Public License
#  along with Gnu Heads.  If not, see <https://www.gnu.org/licenses/>.

from player import *
from table import *
import card
import asyncio
import json
#import logging
import websockets
import sys
from random import randint
import traceback

playerList = {}
tableList = {}
chatHistory = [] #todo: un historique par table
historyLimit = 15

async def register(player):
    playerList[player.getId()] = player

def unregister(player):
    try:
        t=player.unsetTable()
        if not t.getPlayerList():
            del tableList[t.getId()]
        del playerList[player.getId()]
    except Exception:
        pass

async def setPlayerName(player, name):
    player.setName(name)
    #data = json.dumps({"set_player_name": player.getName()})
    #await player.send(data)
    await player.sendPlayerData()

async def joinTable(player, table_id, password):
    if table_id in tableList:
        table = tableList[table_id]
        if table.verifyPassword(password):
            if table.getStatus() == "wait" or table.getStatus() == "end":
                # Add the player to the table
                table.addPlayer(player)
                player.setTable(table)
                player.setPoint(table.getPoint())
                await sendTable(table)
            else:
                await player.send(json.dumps({"msg": "table_play"}))
        elif table == player.getTable():
            await sendTable(table)

async def quitTable(player):
    table = player.unsetTable()
    if table:
        await player.sendPlayerData()
        await player.send(json.dumps({"quit_table": True}))
        await sendTable(table)
        if not table.getPlayerList():
            del tableList[table.getId()]

async def startTable(player):
    table=player.getTable()
    if(table != None):
        if table.getOwner() == player and table.getStatus() in ("wait", "end"):
            await table.start()

async def selectCard(player, card):
    table = player.getTable()
    try:
        # Placing a card is not allowed for the moment
        if debug:
            print(table.getStatus())
        if table.getStatus() != "play":
            return
    except:
        # The player is not playing at a table
        return
        
    if player.setSelectedCard(card):
        try:
            # needed to select the card in the web client
            await player.sendPlayerData()
            # to tell all players that this player selected a card
            await sendTable(table)
            # try to place the selected cards on the table
            await placeCard(table)
        except:
            # Player is not at a table
            print(traceback.format_exc())

async def selectLine(player, line):
    player.setSelectedLine(line)
    table = player.getTable()
    try:
        await placeCard(table)
    except:
        print(traceback.format_exc())

async def placeCard(table):
    placeStatus = await table.placeCard()
    # Wait until all cards are placed client side (animation time)
    if placeStatus == True:
        delay = 2 + len(table.getPlayerList()) * 2
        loop = asyncio.get_running_loop().create_task(placeCardEnd(table, delay))
    else:
        await sendTable(table)
        for p in table.getPlayerList():
            await p.sendPlayerData()

async def placeCardEnd(table, delay=0):
    "Set the status of the table to play and send table and players status"
    await asyncio.sleep(delay)
    if table.getStatus() == "place_card":
        # // start test end of game
        # Do players still have cards? If not, game is finished
        try:
            if len(table.getPlayerList()[0].getHand()) == 0:
                # It will change the table status to 'end' if a score i below 0
                # and send scores to players
                placeCard()
        except:
            pass
        # // stop test end of game
        table.setStatus("play")
        await sendTable(table)
        for p in table.getPlayerList():
            await p.sendPlayerData()

async def disconnect(player):
    player.disconnect()
    await sendTable(player.getTable())
    # If the player didn't reconnect after 3s, it will be deleted
    await asyncio.sleep(10)
    if player.getStatus() == "disconnected":
        t = player.getTable()
        unregister(player)
        await sendTable(t)

async def reconnect(player: Player, data) -> Player:
    "Try to reconnect the player to its former game"

    oldPlayer = playerList.get(int(data.get("player_id", 0)), None)
    try:
        if oldPlayer.getToken() == data.get("player_token", None):
            oldPlayer.reconnect()
            oldPlayer.setWebsocket(player.getWebsocket())
            await oldPlayer.sendPlayerData()
            if oldPlayer.getTableId():
                await sendTable(oldPlayer.getTable())
                if oldPlayer.getTable().getStatus() == "select_line":
                    await oldPlayer.getTable().sendSelectLine(oldPlayer)
            unregister(player)
            return oldPlayer
        else:
            await player.sendPlayerData()
            return player
    except Exception:
        pass
    await player.sendPlayerData()
    return player


async def sendTable(table):
    ################## Replace with table.sendData()
    if not table:
        return
    msg = json.dumps({"table": table.getData()})
    await table.send(msg)
    if table.getStatus() == "end":
        # Send the final result
        msg = json.dumps({"end": table.getPointList()})
        await table.send(msg)

async def createTable(player, data):
    if not player.getTable():
        for i in range(15):
            code = str(randint(1000000,9999999))
            if code not in tableList:
                password=data.get("password", "")
                deckSize=data.get("deck_size",104)
                lineAmount=data.get("line_amount", 4)
                lineLength=data.get("line_length", 6)
                point = data.get("point", 66)
                tableList[code] = Table(player, code, password, deckSize, lineAmount, lineLength, point)
                await joinTable(player, code, password)
                print("Record table", code, "in list. Qty:", len(tableList))
                return
        raise Exception('Unable to find a table number')

async def gnuHeads(websocket, path):
    # Cette fonction est exécutée pour chaque utilisateur
    # Elle se termine lors de sa déconnexion
    player = Player(websocket)
    await register(player)
    try:
        async for message in websocket:
            data = json.loads(message)
            if debug:
                print(data)
            if data.get("select_card", None):
                await selectCard(player, int(data["select_card"]))
            elif data.get("select_line", None) != None:
                await selectLine(player, int(data["select_line"]))
            elif data.get("set_player_name", None):
                await setPlayerName(player, data["set_player_name"])
                await player.sendPlayerData()
            elif data.get("reconnect", None):
                player = await reconnect(player, data["reconnect"])
            elif data.get("create_table", None):
                await createTable(player, data["create_table"])
            elif data.get("join_table", None):
                await joinTable(player, \
                    data["join_table"].get("table_id", None), \
                    data["join_table"].get("table_password", None))
            elif data.get("start_table", None):
                await startTable(player)
            elif data.get("quit_table", None):
                await quitTable(player)
    except:
        print("Client disconnected")
        if debug:
            print(traceback.format_exc())
    finally:
        #await unregister(player)
        await disconnect(player)


try:
    if sys.argv.index("--debug") > 0:
        debug = True
except ValueError:
    debug = False

# Bind to this address
address = "0.0.0.0"

start_server = websockets.serve(gnuHeads, address, 6789)

try:
    print("\nStarting Gnu Heads\n")
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()
except KeyboardInterrupt:
    print ("\nProgram stopped by User\n")
