#!/usr/bin/env python
# This file generates each card in SVG format
# Use this command to generate png files:
# mogrify -format png -density 300 -background transparent -posterize 5 *.svg
from card import *
import os
saveDir = "public/cards/svg"
try:
    os.makedirs(saveDir)
except FileExistsError as error:
    pass


def card(value, head, bgcolor, txtcolor,gnucolor, ptcolor):
    value = str(value)
    svg ="""<svg version="1.1"
baseProfile="full"
width="55" height="86"
xmlns="http://www.w3.org/2000/svg"
viewBox="0 0 57 88" preserveAspectRatio="xMidYMid slice" class="card">
        <rect x="1" y="1" rx="4" ry="4" width="55" height="86"
      style="fill:lightgrey;stroke:black;stroke-width:1" />
<defs>
    <filter id="shadow">
      <feDropShadow dx="1.4" dy="1" stdDeviation="1.3"
        flood-color="black" flood-opacity="0.5" />
    </filter>
</defs>
<rect x="1" y="1" rx="4" ry="4" width="55" height="86"
  style="fill:white;" />
    <rect x="6" y="11" rx="4" ry="4" width="45" height="66"
  style="fill:{2};" />
    <rect x="1" y="1" rx="4" ry="4" width="55" height="86"
  style="stroke:black;stroke-width:1; fill:none;" />

  <path style="stroke:black; fill:{3};" d="m 51.3,14.8 c -2.2,0.5 -0.8,3.2 -0.9,4.7 -0.2,1.7 -1.7,3.4 -3.5,3.1 -2.1,-0.1 -4,-1.4 -6,-1.9 -1.7,-0.7 -3.5,-1.9 -5.3,-1.1 -1.7,0.6 -3.9,1 -4.7,2.8 -0.5,1.8 -3.1,2.8 -4.3,1 -0.9,-1.2 -1.6,-2.6 -3.2,-3.1 -2.2,-1.1 -4.8,-1.4 -7.1,-0.3 -2.5,1 -4.9,2.7 -7.72,2.6 -2.15,-0.5 -3.31,-3.3 -2.12,-5.2 0.64,-2 -2.27,-3 -2.9,-0.9 -0.72,1.5 -1.47,3 -1.95,4.6 -0.41,1.6 -0,3.6 1.54,4.4 2.7,1.8 6.11,1.9 9.25,1.9 1.2,-0.5 3,0.7 1.3,1.7 -1.4,1.2 -3.4,1.2 -5.2,1.4 -1.87,0.2 -3.16,1.6 -4.6,2.6 -1.54,0.8 -1.52,3.3 0.5,3.2 1.57,-0.1 4.66,-1.2 4.66,1.4 -0.25,1.6 -0.75,3.1 -0.56,4.8 0.19,2.9 1.44,5.7 3.5,7.8 2,3 4.8,5.5 8.3,6.7 2,0.4 3,2.5 1.6,4.2 -0.8,1 0.4,3 1.3,1.6 0.4,-1.4 2,-1.2 2,0.3 0.1,1.9 -0.3,4 0.6,5.7 1.1,1.8 2.4,-0.4 2,-1.7 -0.1,-2 3.1,-2.3 3.1,-0.2 -0.5,1.6 2.4,3.2 2.4,1 0,-1.7 -2.6,-4 -0.6,-5.3 1.3,0.7 3.3,1 3.3,-1.1 -0.1,-1.5 -2.1,-3.6 0.2,-4.3 1.7,-0.6 3.1,-1.5 4.5,-2.6 3.8,-2.8 6.6,-7.1 7.1,-11.9 0.3,-1.8 0.1,-3.6 -0.1,-5.4 0.4,-1.9 3,-1.1 4.2,-0.6 2.2,0.4 3.2,-3 1,-3.6 -1.6,-0.8 -3.3,-1.5 -4.9,-2.2 -1.7,-0.7 -3.7,-0.5 -5,-1.9 -1.4,-1.7 1.8,-1.4 2.8,-1.5 1.9,-0.1 3.8,-0.4 5.7,-0.7 1.8,-0.4 3.9,-1.9 3.6,-4 -0.1,-2.9 -1.5,-5.6 -3.3,-7.8 -0.1,-0.1 -0.3,-0.2 -0.5,-0.2 z" />
  
  <text x="28" y="54" fill="white" stroke="white" stroke-width="7" style="font: bold 25pt Vemana2000" text-anchor="middle">{0}</text>
  <text x="28" y="54" fill="{1}" stroke="black" style="font: bold 25pt Vemana2000" text-anchor="middle">{0}</text>
  <text x="10" y="8" fill="black" style="font: bold 5pt sans-serif" text-anchor="middle">{0}</text>
  <text x="47" y="8" fill="black" style="font: bold 5pt sans-serif" text-anchor="middle">{0}</text>
  <text x="10" y="8" fill="black" style="font: bold 5pt sans-serif" text-anchor="middle" transform="translate(57,88) rotate(180)">{0}</text>
  <text x="47" y="8" fill="black" style="font: bold 5pt sans-serif" text-anchor="middle" transform="translate(57,88) rotate(180)">{0}</text>
  <g class="points" stroke="black" stroke-width="0.5" fill="brown">
  <g fill="{4}">""".format(value, txtcolor, bgcolor, gnucolor, ptcolor)

    if head == 2:
        points = '<circle cx="25" cy="5" r="2" />' +\
        '<circle cx="31" cy="5" r="2" />'
    elif head == 3:
        points = '<circle cx="22" cy="5" r="2" />' +\
        '<circle cx="28" cy="5" r="2" />' +\
        '<circle cx="34" cy="5" r="2" />'
    elif head == 5:
        points = '<circle cx="22" cy="5" r="2" />' +\
        '<circle cx="28" cy="5" r="2" />' +\
        '<circle cx="34" cy="5" r="2" />' +\
        '<circle cx="25" cy="11" r="2" />' +\
        '<circle cx="31" cy="11" r="2" />'
    elif head == 7:
        points = '<circle cx="19" cy="5" r="2" />' +\
        '<circle cx="25" cy="5" r="2" />' +\
        '<circle cx="31" cy="5" r="2" />' +\
        '<circle cx="37" cy="5" r="2" />' +\
        '<circle cx="22" cy="11" r="2" />' +\
        '<circle cx="28" cy="11" r="2" />' +\
        '<circle cx="34" cy="11" r="2" />'
    else:
        points = '<circle cx="28" cy="5" r="2" />'

    svg += points + '<g transform="translate(57,88) rotate(180)">' + points + '</g>'
    svg += """      </g>
    </g>
    </svg>"""
    return svg
    
html = """<html>
  <head>
    <style>
      img{
        width: 10%;
      }
      body{
        margin: 0;
        padding: 0;
      }
    </style>
  </head>
  <body>
"""

for value in range(1,262):
    head = getPoint(value)
    if head == 2:
        bgcolor = "#9E92D5"
        txtcolor = "#2E88A7"
        gnucolor = "#412F96"
        ptcolor = "#2E88A7"
    elif head == 3:
        bgcolor = "#72CF63"
        txtcolor = "#FFFF00"
        gnucolor = "#248313"
        ptcolor = "#FFFF00"
    elif head == 5:
        bgcolor = "#DE0008"
        txtcolor = "#45A335"
        gnucolor = "#412F96"
        ptcolor = "#248313"
    elif head == 7:
        bgcolor = "#FFFF00"
        txtcolor = "#DE0008"
        gnucolor = "#412F96"
        ptcolor = "#DE0008"
    else:
        bgcolor = "#FFFFFF"
        txtcolor = "#AD82AA"
        gnucolor = "#412F96"
        ptcolor = "#AD82AA"

    filename = str(value) + ".svg"
    myfile = open(os.path.join(saveDir, filename), 'w')
    myfile.write(card(value, head, bgcolor, txtcolor,gnucolor, ptcolor))
    myfile.close()
    html += "<img src='" + str(value) + ".svg'>"


html += """
</body>
</html>"""

myfile = open(os.path.join(saveDir, "card_generator.htm"), 'w')
myfile.write(html)
myfile.close()
